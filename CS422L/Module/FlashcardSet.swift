//
//  FlashcardSet.swift
//  CS422L
//
//  Created by Daniela Chavez on 2/23/22.
//

import Foundation
import CoreData
import UIKit


struct FlashcardSetOBJ: Codable {
    let id: Int16?
    let title: String?
    
    enum CodingKeys: String, CodingKey {
        case title, id
    }
}

