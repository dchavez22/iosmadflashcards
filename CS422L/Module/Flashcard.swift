//
//  Flashcard.swift
//  CS422L
//
//  Created by Daniela Chavez on 2/24/22.
//

import Foundation
import CoreData
import UIKit


struct FlashcardOBJ: Codable {
    let id: Int16?
    let term: String?
    let definition: String?
    
    enum CodingKeys: String, CodingKey {
        case term, definition, id
    }
}
