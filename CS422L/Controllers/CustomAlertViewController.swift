//
//  CustomAlertViewController.swift
//  CS422L
//
//  Created by Daniela Chavez on 2/13/22.
//

import UIKit
import CoreData

class CustomAlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    var parentVC: FlashcardSetDetailViewController!
    @IBOutlet var alertDef: UITextField!
    @IBOutlet var alertTerm: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertTerm.text = parentVC.flashcards[parentVC.selectedIndex].term
        alertDef.text = parentVC.flashcards[parentVC.selectedIndex].definition
        
    }
    

    @IBAction func saveButton(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        do{
            parentVC.flashcards[parentVC.selectedIndex].term = alertTerm.text ?? ""
            parentVC.flashcards[parentVC.selectedIndex].definition = alertDef.text ?? ""
            
            try context.save()
            parentVC.tableView.reloadData()
            self.dismiss(animated: true, completion: {})
        }catch{
            print(error)
        }
    
       
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        do{
            context.delete(parentVC.flashcards[parentVC.selectedIndex])
            parentVC.flashcards.remove(at: parentVC.selectedIndex)
            parentVC.tableView.reloadData()
            self.dismiss(animated: true, completion: {})
        }
        
    }
}
