//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit
import CoreData


class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {
    var flashcardSets: [FlashcardSetEntity] = []

    @IBOutlet weak var collectionView: UICollectionView!
    
    
   // var sets: [FlashcardSet] = [FlashcardSet]()
    var buttonClicked = 0
    var idIncrement = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //sets = FlashcardSet.getHardCodedCollection()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
      //clearData()
       loadData()
        
    
    }


    @IBAction func addSetButtonClicked(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
      
        let newSet = NSEntityDescription.insertNewObject(forEntityName: "FlashcardSetEntity", into: context) as! FlashcardSetEntity
        newSet.id = Int16(idIncrement)
        newSet.title = "Title \(flashcardSets.count + 1)"
        do{
            
            try context.save()
            self.flashcardSets.append(newSet)
            collectionView.reloadData()
        }catch{
            print(error)
        }
        idIncrement+=1
        
      let newIndex = IndexPath(item: flashcardSets.count - 1, section: 0)
//        collectionView.insertItems(at: [newIndex])
     collectionView.scrollToItem(at: newIndex, at: .bottom, animated: true)
//        buttonClicked += 1
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return number of items
        return flashcardSets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as? FlashcardSetCollectionCell
        //cell.textLabel.text = sets[indexPath.item].title
        {
            let set = flashcardSets[indexPath.row]
            cell.textLabel.text = set.title
            return cell
        }
    
        //setup cell display here
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToDetail", sender: self)
        
    }
    
    func clearData(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashcardSetEntity")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do{
            try context.execute(deleteRequest)
        }catch let error as NSError {
            print(error)
        }
        UserDefaults.standard.set(false, forKey: "isDownloaded")
        print("Data cleared")
    }
    
    func loadData(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        if (UserDefaults.standard.bool(forKey: "isDownloaded")){
            print("data already downloaded")
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashcardSetEntity")
            do {
                let sets = try context.fetch(fetchRequest) as! [FlashcardSetEntity]
                updateData(sets: sets)
            }catch{
                print(error)
            }
            
        }else{
            var sets: [FlashcardSetEntity] = []
            UserDefaults.standard.set(true, forKey: "isDownloaded")
               for i in 1...10{
                    let newSet = NSEntityDescription.insertNewObject(forEntityName: "FlashcardSetEntity", into: context) as! FlashcardSetEntity
                newSet.id = Int16(idIncrement)
                   newSet.title = "Set \(i)"
                    sets.append(newSet)
                   idIncrement+=1
                }
                do {
                    try context.save()
                }
                catch{
                    print(error)
                }
            self.updateData(sets: sets)
        }
    }
            
        func updateData(sets: [FlashcardSetEntity]) {
                self.flashcardSets = sets
                self.collectionView.reloadData()
        }

    
    
}

