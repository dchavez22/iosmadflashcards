//
//  FlashcardSetDetailViewController.swift
//  CS422L
//
//  Created by Daniela Chavez on 2/3/22.
//

import Foundation
import UIKit
import CoreData

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var flashcards: [FlashcardEntity] = []
    var idIncrement = 0
    
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addButton.setTitle("Add", for: .normal)
        studyButton.setTitle("Study", for: .normal)
        deleteButton.setTitle("Delete", for: .normal)
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //clearData()
        loadData()
     
    }
    
    
    @IBAction func addButtonClicked(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let newCard = NSEntityDescription.insertNewObject(forEntityName: "FlashcardEntity", into: context) as! FlashcardEntity
        newCard.id = Int16(idIncrement)
        newCard.term = "term \(flashcards.count + 1)"
        newCard.definition = "definiton \(flashcards.count + 1)"
        do{
            
            try context.save()
            self.flashcards.append(newCard)
            tableView.reloadData()
        }catch{
            print(error)
        }
        idIncrement+=1
        
        let newIndex = IndexPath(row: flashcards.count - 1, section: 0)
        //tableView.insertRows(at: [newIndex], with: .automatic)
        tableView.scrollToRow(at: newIndex, at: .bottom, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flashcards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as? FlashcardTableCell
        {
            let card = flashcards[indexPath.row]
            cell.label.text = card.term
            return cell
        }
        
        return UITableViewCell()
    }
    
    func createAlert(indexPath: IndexPath)
    {
        let alert = UIAlertController(title: flashcards[indexPath.row].term, message: flashcards[indexPath.row].definition, preferredStyle: .alert)
        selectedIndex = indexPath.row
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            alert.dismiss(animated: true, completion: {})
            self.createCustomAlert()
        }))
        self.present(alert, animated: true)
    }
    
    func createCustomAlert(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        alertVC.parentVC = self
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: true, completion: nil)
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        createAlert(indexPath: indexPath)
    }

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func clearData(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashcardEntity")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do{
            try context.execute(deleteRequest)
        }catch let error as NSError {
            print(error)
        }
        UserDefaults.standard.set(false, forKey: "isDownloaded")
        print("Data cleared")
    }
    
    func loadData(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        if (UserDefaults.standard.bool(forKey: "isCardsDownloaded")){
            print("cards data already downloaded")
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashcardEntity")
            do {
                let cards = try context.fetch(fetchRequest) as! [FlashcardEntity]
                updateData(cards: cards)
            }catch{
                print(error)
            }
            
        }else{
            var cards: [FlashcardEntity] = []
            UserDefaults.standard.set(true, forKey: "isCardsDownloaded")
               for i in 1...10{
                    let newCard = NSEntityDescription.insertNewObject(forEntityName: "FlashcardEntity", into: context) as! FlashcardEntity
                   newCard.id = Int16(idIncrement)
                   newCard.term = "term \(i)"
                   newCard.definition = "definiton \(i)"
                   cards.append(newCard)
                   idIncrement+=1
                }
                do {
                    try context.save()
                }
                catch{
                    print(error)
                }
            self.updateData(cards: cards)
        }
    }
            
        func updateData(cards: [FlashcardEntity]) {
                self.flashcards = cards
                self.tableView.reloadData()
        }
}
